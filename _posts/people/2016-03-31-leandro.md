---
layout: page
title:  "Leandro Costa Valadão"
subheadline:  "M.Sc. Student"
teaser: "I'm a person with strong personality, trying to optimize ideas for better processes."
categories:
    - people
    - msc
tags:
    - people
    - master
    - msc
image:
   thumb: "leandro.jpg"
breadcrumb: true
---
![John's photo](/images/leandro.jpg)

### Resume
I'm Leandro Costa Valadão, born in Patos de Minas, Minas Gerais, Brazil. I study since I was 6 years old, always in public schools, I was like to study and learn new things. My family has much teachers, like my father, brother and my sister, and I like to be one some day. I did my graduation on public University Federal of Viçosa in Minas Gerais, I was 17 when i start my graduation. When i doing my graduation I try to do as much as I can to learn and teach, doing projects with my teachers and teaching lessons about marathons programming.

In college I learn much about Web Systems and how can they be versatile, by doing 3 years of projects with newst tecnologies in Web, with this time I decide to try an Master's Degree in Web area.

Nowadays I'm studying Semantic Web on University of São Paulo (ICMC-USP), trying to make versatile systems, that can adaptate to very diferent kinds of areas wich need an Expert System to help humans to make more optimized decisions. In addition I was an Systems Analyst on an company that make systems to improve the capacity of reading and writing of a kids and teenagers, this system makes pedagogical messages and statistics to help learning.


### Academic Background

M.Sc. Student in Computer Sciences at the [University of Sao Paulo](http://www.icmc.usp.br/Portal/) (ICMC-USP) 
* Institute: Institute of Mathematical and Computer Sciences
* Research lab.: Intermídia Interactive Web and Multimedia Systems
* Research area: Semantic Web
• Project: Semantic web technologies in the generation of Graphical User Interfaces to support Agricultural Decision Systems, case SustenAgro

Information Systems at the [Universidade Federal de Viçosa](http://www.ufv.br/)(UFV). 
* Institute: Institute of Engineering and Computation
* Research area: Web Development
* Graduation date: 01/24/2014
* Country: Brazil

### Research Interests
* Web Development
* Software engineering modeling
* Educational Systems
* Natural Language Processing
* Semantic Web 
* Linked Data
* Ontologies


### Publications
* [Lattes CV]( http://lattes.cnpq.br/3131965652341737) 

### Languages
* Portuguese:	Native
* English:		Intermediate



### Relevant skills and experience
* Programming languages: Python, Java, Groovy, JavaScript, C
* Databases: MySql
* IDE's: Aptana Studio, Netbeans, Sublime Text
* General programming methods: OO, Agile methods: Scrum

### Employment experiences
* Systems Analyst on Centro de Autoria e Cultura Ltda
